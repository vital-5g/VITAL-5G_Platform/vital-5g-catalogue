![vital-5g-logo](https://www.vital5g.eu/wp-content/uploads/2020/12/vital-logo-web.png) 
 # Network Applications, Services and  Experiment Catalogue


[VITAL-5G D2.1](https://www.vital5g.eu/wp-content/uploads/2022/01/VITAL5G_D2.1_Initial_NetApps_blueprints_and_Open_Repository_design_Final.pdf) 

*The Network Applications, Services and Experiments Catalogue is the component responsible for storing Network Application packages and blueprints, Vertical Service Blueprints and Descriptors (VSB and VSD), Experiment Blueprints and Descriptors (ExpB and ExpD). The Catalogue maintains the synchronization with the NFVO catalogues at the three VITAL-5G sites, to keep the map between NetApp and Vertical Service Blueprints and the corresponding VNF packages and Network Service Descriptors (NSD), respectively.*

This repository contains the API specification of the VITAL-5G Catalogue module, while the software and documentation repository is available in  [Main repository](https://gitlab.com/vital-5g/VITAL-5G_Platform/OpenOnlineRepository/-/tree/main) 

* [API](API/): OpenAPI specification of the interfaces by this module and Postman collections


## License
This module has been developed by [Nextworks](www.nextworks.it) and licensed under the open source [Apache License v2.0](https://www.apache.org/licenses/LICENSE-2.0)



